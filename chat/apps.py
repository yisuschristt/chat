from django.apps import AppConfig

# Nombre de la app


class ChatConfig(AppConfig):
    name = 'chat'
