from django.contrib.auth import get_user_model
from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
import json
from .models import Message, Chat, Contact
from .views import get_last_10_messages, get_user_contact, get_current_chat

# Se llaman los datos del modelo Usuario.
User = get_user_model()

# Se inicializa la Clase del consumidor


class ChatConsumer(WebsocketConsumer):

    # Se define el metodo de recopilacion de mensajes previos MAX:10
    def fetch_messages(self, data):
        messages = get_last_10_messages(data['chatId'])
        content = {
            'command': 'messages',
            'messages': self.messages_to_json(messages)
        }
        self.send_message(content)

    # Se define el metodo para crear y configurar un nuevo mensaje, en donde se necesita
    # la informacion del author que seria el usuario y el contenido que seria el mensaje
    def new_message(self, data):
        user_contact = get_user_contact(data['from'])
        message = Message.objects.create(
            contact=user_contact,
            content=data['message'])
        current_chat = get_current_chat(data['chatId'])
        current_chat.messages.add(message)
        current_chat.save()
        content = {
            'command': 'new_message',
            'message': self.message_to_json(message)
        }
        return self.send_chat_message(content)

    # Metodo para serializar los mensajes previos que se van a renderizar
    def messages_to_json(self, messages):
        result = []
        for message in messages:
            result.append(self.message_to_json(message))
        return result

    # Metodo para serializar los mensajes nuevos que se enviaran, se arma el json con id, autor, contenido y marca del tiempo.
    def message_to_json(self, message):
        return {
            'id': message.id,
            'author': message.contact.user.username,
            'content': message.content,
            'timestamp': str(message.timestamp)
        }
    # Comandos que se utilizan para definir el tipo de accion que realizara cada proceso.
    commands = {
        'fetch_messages': fetch_messages,
        'new_message': new_message
    }

    # Metodo para hacer la conexion con el canal por donde se transferiran los mensajes.
    def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'chat_%s' % self.room_name
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name
        )
        # Metodo para intentar acceptar la conexion con los parametros que se estan enviando.
        self.accept()

    # Metodo para cerrar la conexion existente.
    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name
        )

    # Metodo para la recepcion de mensajes y para prepararlos en formato json, listos para renderizar.
    def receive(self, text_data):
        data = json.loads(text_data)
        self.commands[data['command']](self, data)

    # Se define el metodo para enviar el mensaje a traves del canal cuando ya ha sido configurado en el Metodo new_message()
    def send_chat_message(self, message):
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            {
                'type': 'chat_message',
                'message': message
            }
        )

    # Metodo de serializacion json.dumps para la renderizacion
    def send_message(self, message):
        self.send(text_data=json.dumps(message))

    # Metodo de serializacion json.dumps para enviar el mensaje
    def chat_message(self, event):
        message = event['message']
        self.send(text_data=json.dumps(message))
