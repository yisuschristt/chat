from django.contrib.auth import get_user_model
from django.db import models

# Se llama al modelo User
User = get_user_model()

# Se define la clase Contact, que se usara a lo largo de los procesos, incluyendo el consumers.py


class Contact(models.Model):
    user = models.ForeignKey(
        User, related_name='friends', on_delete=models.CASCADE)
    friends = models.ManyToManyField('self', blank=True)

    # Metodo usado para representar los objetos de
    # una clase como string, tambien se utiliza para debug cuando los miembros de una clase necesitan ser chequeados
    def __str__(self):
        return self.user.username

# Se define la clase Message, que se usara a lo largo de los procesos, incluyendo el consumers.py


class Message(models.Model):
    contact = models.ForeignKey(
        Contact, related_name='messages', on_delete=models.CASCADE)
    content = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)

    # Metodo usado para representar los objetos de
    # una clase como string, tambien se utiliza para debug cuando los miembros de una clase necesitan ser chequeados
    def __str__(self):
        return self.contact.user.username

# Se define la clase Chat, que se usara a lo largo de los procesos, incluyendo el consumers.py


class Chat(models.Model):
    participants = models.ManyToManyField(
        Contact, related_name='chats', blank=True)
    messages = models.ManyToManyField(Message, blank=True)

    # Metodo usado para representar los objetos de
    # una clase como string, tambien se utiliza para debug cuando los miembros de una clase necesitan ser chequeados
    def __str__(self):
        return "{}".format(self.pk)
