# chat/routing.py
from django.urls import re_path

from .consumers import ChatConsumer
# Envia el el path con el nombre de la sala y el Consumer para ejecutar los procesos
websocket_urlpatterns = [
    re_path(r'^ws/chat/(?P<room_name>[^/]+)/$', ChatConsumer),
]
