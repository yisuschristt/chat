import * as actionTypes from "../actions/actionTypes";
import { updateObject } from "../utility";
//Se definen las constantes del estado inicial de la app
const initialState = {
  token: null,
  username: null,
  error: null,
  loading: false
};
//Se modifican las constantes del estado inicial dependiendo de la accion que se requiera 

//Al iniciar la sesion mientras se validan los datos, se estara cargando
const authStart = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};
//Si logra iniciar sesion desaparece el cargando y se almacena el token y username
const authSuccess = (state, action) => {
  return updateObject(state, {
    token: action.token,
    username: action.username,
    error: null,
    loading: false
  });
};
//Si falla el inicio de sesion mostrara el error correspondiente
const authFail = (state, action) => {
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};
//Si cierra sesion eliminara los valores almacenados en el local storage y por lo tanto la sesion no existira.
const authLogout = (state, action) => {
  return updateObject(state, {
    token: null,
    username: null
  });
};
//Switch and cases utilizadas para asignar un tipo de accion dependiendo de la interaccion con la app.
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.AUTH_START:
      return authStart(state, action);
    case actionTypes.AUTH_SUCCESS:
      return authSuccess(state, action);
    case actionTypes.AUTH_FAIL:
      return authFail(state, action);
    case actionTypes.AUTH_LOGOUT:
      return authLogout(state, action);
    default:
      return state;
  }
};

export default reducer;
