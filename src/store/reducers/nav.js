import * as actionTypes from "../actions/actionTypes";
import { updateObject } from "../utility";

//El popup inicializa invisible hasta ser ejecutado
const initialState = {
  showAddChatPopup: false
};
//Accion que modifica el estado de invisible a visible
const openAddChatPopup = (state, action) => {
  return updateObject(state, { showAddChatPopup: true });
};
//Accion que modifica el estado de visible a invisible
const closeAddChatPopup = (state, action) => {
  return updateObject(state, { showAddChatPopup: false });
};
//Aqui se define con los reducers el tipo de accion a ejecutar dependiendo de lo que se haya ejecutado.
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.OPEN_ADD_CHAT_POPUP:
      return openAddChatPopup(state, action);
    case actionTypes.CLOSE_ADD_CHAT_POPUP:
      return closeAddChatPopup(state, action);
    default:
      return state;
  }
};

export default reducer;
