import * as actionTypes from "../actions/actionTypes";
import { updateObject } from "../utility";

const initialState = {
  messages: [],
  chats: []
};
//Comando para añadir nuevo mensaje
const addMessage = (state, action) => {
  return updateObject(state, {
    messages: [...state.messages, action.message]
  });
};
//Comando que se utilizara para cargar el arreglo de mensajes previos.
const setMessages = (state, action) => {
  return updateObject(state, {
    messages: action.messages.reverse()
  });
};
//Comando para obtener los chats del usuario iniciado.
const setChats = (state, action) => {
  return updateObject(state, {
    chats: action.chats
  });
};

//Aqui se define con los reducers el tipo de accion a ejecutar dependiendo de lo que se haya ejecutado.
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.ADD_MESSAGE:
      return addMessage(state, action);
    case actionTypes.SET_MESSAGES:
      return setMessages(state, action);
    case actionTypes.GET_CHATS_SUCCESS:
      return setChats(state, action);
    default:
      return state;
  }
};

export default reducer;
