import * as actionTypes from "./actionTypes";
//Abrir el popup para añadir un nuevo chat
export const openAddChatPopup = () => {
  return {
    type: actionTypes.OPEN_ADD_CHAT_POPUP
  };
};
//Cerrar el popup despues de añadir el nuevo chat.
export const closeAddChatPopup = () => {
  return {
    type: actionTypes.CLOSE_ADD_CHAT_POPUP
  };
};
