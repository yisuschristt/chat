import React from "react";
import { Modal } from "antd";
import Form from "./Form";

// Clase que abarca la renderizacion del formulario donde se inicia sesion y se registran los usuarios.

class AddChatModal extends React.Component {
  render() {
    return (
      <Modal
        centered
        footer={null}
        visible={this.props.isVisible}
        onCancel={this.props.close}
      >
        <Form />
      </Modal>
    );
  }
}

export default AddChatModal;
