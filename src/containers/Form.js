import React from "react";
import { Form, Button, Select } from "antd";
import axios from "axios";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import * as navActions from "../store/actions/nav";
import * as messageActions from "../store/actions/message";
import { HOST_URL } from "../settings";

const FormItem = Form.Item;

// Si algun campo tiene un error, mostrara el reloj
function hasErrors(fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
}
//DUDA AQUI
class HorizontalAddChatForm extends React.Component {
  state = {
    usernames: [],
    error: null
  };
//Al efectuar un cambio el estado cambia y se le asignan los valores.
  handleChange = value => {
    this.setState({
      usernames: value
    });
  };
//Metodo que realiza una accion cuando el componente esta montado
  componentDidMount() {
    //Validar los campos del formulario
    this.props.form.validateFields();
  }
//Metodo que ejecuta una accion al darle al boton que tiene el type submit, en este caso creara la sala de chat que hemos tipeado.
  handleSubmit = e => {
    const { usernames } = this.state;
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const combined = [...usernames, this.props.username];
        axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
        axios.defaults.xsrfCookieName = "csrftoken";
        axios.defaults.headers = {
          "Content-Type": "application/json",
          Authorization: `Token ${this.props.token}`
        };
        axios
          .post(`${HOST_URL}/chat/create/`, {
            messages: [],
            participants: combined
          })
          .then(res => {
            this.props.history.push(`/${res.data.id}`);
            this.props.closeAddChatPopup();
            this.props.getUserChats(this.props.username, this.props.token);
          })
          .catch(err => {
            console.error(err);
            this.setState({
              error: err
            });
          });
      }
    });
  };
//Metodo que renderiza constantes necesarias para el formulario del componente.
  render() {
    const {
      getFieldDecorator,
      getFieldsError,
      getFieldError,
      isFieldTouched
    } = this.props.form;
    //Metodo de validacion para el campo de username al momento de tipearlo para chatear con el.
    const userNameError =
      isFieldTouched("userName") && getFieldError("userName");
    return (
      //Formulario horizontal para tipear el username de la persona con la que queremos chatear.
      <Form layout="inline" onSubmit={this.handleSubmit}>
        {this.state.error ? `${this.state.error}` : null}
        <FormItem
          validateStatus={userNameError ? "error" : ""}
          help={userNameError || ""}
        >
          {getFieldDecorator("userName", {
            rules: [
              {
                required: true,
                message:
                  "Please input the username of the person you want to chat with"
              }
            ]
          })(
            <Select
              mode="tags"
              style={{ width: "100%" }}
              placeholder="Tags Mode"
              onChange={this.handleChange}
            >
              {[]}
            </Select>
          )}
        </FormItem>
        <FormItem>
          <Button
            type="primary"
            htmlType="submit"
            disabled={hasErrors(getFieldsError())}
          >
            Start a chat
          </Button>
        </FormItem>
      </Form>
    );
  }
}

const AddChatForm = Form.create()(HorizontalAddChatForm);
//Metodo que asigna propiedados al estado del componente.
const mapStateToProps = state => {
  return {
    token: state.auth.token,
    username: state.auth.username
  };
};
//Metodo que ejecuta una accion sobre las propiedades del componente.
const mapDispatchToProps = dispatch => {
  return {
    closeAddChatPopup: () => dispatch(navActions.closeAddChatPopup()),
    getUserChats: (username, token) =>
      dispatch(messageActions.getUserChats(username, token))
  };
};
//Metodo para retornar con la ruta exacta y la conexion exitosa al chat donde se desea hablar
export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(AddChatForm)
);
