import React from "react";
import { connect } from "react-redux";
import WebSocketInstance from "../websocket";
import Hoc from "../hoc/hoc";

//Clase utilizada para crear el componente del chat 
class Chat extends React.Component {
  //El estado mensaje inicializa vacio ya que es nuevo chat
  state = { message: "" };
//Metodo para inicializar el chat 
  initialiseChat() {
    //Metodo para esperar por la conexion
    this.waitForSocketConnection(() => {
      WebSocketInstance.fetchMessages(
        this.props.username,
        this.props.match.params.chatID
      );
    });
    WebSocketInstance.connect(this.props.match.params.chatID);
  }
  //Contructor en donde se inicializan las propiedades y se ordena inicializar el chat
  constructor(props) {
    super(props);
    this.initialiseChat();
  }
  //Metodo que valida si la conexion al socket esta hecha.
  waitForSocketConnection(callback) {
    const component = this;
    setTimeout(function() {
      if (WebSocketInstance.state() === 1) {
        console.log("Connection is made");
        callback();
        return;
      } else {
        console.log("wait for connection...");
        component.waitForSocketConnection(callback);
      }
    }, 100);
  }
  //Metodo para cambiar y almacenar el mensaje interactivamente, cada vez que cambia DUDA AQUI!!!
  messageChangeHandler = event => {
    this.setState({ message: event.target.value });
  };

  //Metodo Utilizado al enviar el mensaje con el contenido, el usuario y el iddelchat.
  sendMessageHandler = e => {
    e.preventDefault();
    const messageObject = {
      from: this.props.username,
      content: this.state.message,
      chatId: this.props.match.params.chatID
    };
    //Metodo para inicializar en vacio para un nuevo mensaje
    WebSocketInstance.newChatMessage(messageObject);
    this.setState({ message: "" });
  };
  //Metodo para ponerle hora o fecha a mensajes enviados.
  renderTimestamp = timestamp => {
    let prefix = "";
    const timeDiff = Math.round(
      (new Date().getTime() - new Date(timestamp).getTime()) / 60000
    );
    if (timeDiff < 1) {
      // less than one minute ago
      prefix = "just now...";
    } else if (timeDiff < 60 && timeDiff > 1) {
      // less than sixty minutes ago
      prefix = `${timeDiff} minutes ago`;
    } else if (timeDiff < 24 * 60 && timeDiff > 60) {
      // less than 24 hours ago
      prefix = `${Math.round(timeDiff / 60)} hours ago`;
    } else if (timeDiff < 31 * 24 * 60 && timeDiff > 24 * 60) {
      // less than 7 days ago
      prefix = `${Math.round(timeDiff / (60 * 24))} days ago`;
    } else {
      prefix = `${new Date(timestamp)}`;
    }
    return prefix;
  };
  //Metodo en donde se definen los que serian mensajes enviados y respuestas.
  renderMessages = messages => {
    const currentUser = this.props.username;
    return messages.map((message, i, arr) => (
      <li
        key={message.id}
        style={{ marginBottom: arr.length - 1 === i ? "300px" : "15px" }}
        className={message.author === currentUser ? "sent" : "replies"}
      >
        <img
          src="http://emilcarlsson.se/assets/mikeross.png"
          alt="profile-pic"
        />
        <p>
          {message.content}
          <br />
          <small>{this.renderTimestamp(message.timestamp)}</small>
        </p>
      </li>
    ));
  };
  //Metodo para cargar la vista desde el ultimo mensaje.
  scrollToBottom = () => {
    this.messagesEnd.scrollIntoView({ behavior: "smooth" });
  };
  //Metodo que se acciona al estar montado el componente.
  componentDidMount() {
    this.scrollToBottom();
  }
  //Metodo que se acciona al actualizar el componente.
  componentDidUpdate() {
    this.scrollToBottom();
  }
//Metodo que recibe propiedades y compara si se hace cambio de chat, ya que tendria que cerrar la conexion y volver a abrirla con las nuevas propiedades
  componentWillReceiveProps(newProps) {
    if (this.props.match.params.chatID !== newProps.match.params.chatID) {
      WebSocketInstance.disconnect();
      this.waitForSocketConnection(() => {
        WebSocketInstance.fetchMessages(
          this.props.username,
          newProps.match.params.chatID
        );
      });
      WebSocketInstance.connect(newProps.match.params.chatID);
    }
  }
  //Metodo utilizado para renderizar los componentes
  render() {
    return (
      <Hoc>
        <div className="messages">
          <ul id="chat-log">
            {this.props.messages && this.renderMessages(this.props.messages)}
            <div
              style={{ float: "left", clear: "both" }}
              ref={el => {
                this.messagesEnd = el;
              }}
            />
          </ul>
        </div>
        {/* Aqui esta la caja de texto donde se envian los mensajes  */}
        <div className="message-input">
          <form onSubmit={this.sendMessageHandler}>
            <div className="wrap">
              <input
                onChange={this.messageChangeHandler}
                value={this.state.message}
                required
                id="chat-message-input"
                type="text"
                placeholder="Write your message..."
              />
              <i className="fa fa-paperclip attachment" aria-hidden="true" />
              <button id="chat-message-submit" className="submit">
                <i className="fa fa-paper-plane" aria-hidden="true" />
              </button>
            </div>
          </form>
        </div>
      </Hoc>
    );
  }
}
//Metodo que asigna propiedados al estado del componente.
const mapStateToProps = state => {
  return {
    username: state.auth.username,
    messages: state.message.messages
  };
};
//Metodo para retornar con la ruta exacta y la conexion exitosa al chat donde se desea hablar
export default connect(mapStateToProps)(Chat);
