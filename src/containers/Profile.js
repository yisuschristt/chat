import React from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import Hoc from "../hoc/hoc";

class Profile extends React.Component {
  // Metodo para validacion si existe sesion abierta o token
  render() {
    if (this.props.token === null) {
      return <Redirect to="/" />;
    }
    // Retorna el perfil de contacto y valida que no coloque el usuario si se encuentra en null.
    return (
      <div className="contact-profile">
        {this.props.username !== null ? (
          <Hoc>
            <img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="" />
            <p>{this.props.username}</p>
            <div className="social-media">
              <i className="fa fa-facebook" aria-hidden="true" />
              <i className="fa fa-twitter" aria-hidden="true" />
              <i className="fa fa-instagram" aria-hidden="true" />
            </div>
          </Hoc>
        ) : null}
      </div>
    );
  }
}
// Se realiza la conexion retornando username y token
const mapStateToProps = state => {
  return {
    username: state.auth.username,
    token: state.auth.token
  };
};
//Metodo para conectar el websocket, pasandole las propiedades en la funcion.
export default connect(mapStateToProps)(Profile);
